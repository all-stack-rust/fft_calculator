use rustfft::num_complex::Complex;
use rustfft::FFTplanner;

pub struct FftData {
    pub fft_data: Vec<Complex<f32>>,
    pub x_data: Vec<f32>
}

impl FftData {

    pub fn from_slice(data: &[f32], dt: f32) -> FftData{

        let len = data.len() as f32;

        let x_data = (0..(data.len())).map(|x| {
            ((x as f32 ) / dt) / len
        }).collect();

        let mut fft_output = FftData {
            fft_data: vec![Complex::new(0.0f32, 0.0f32); data.len()],
            x_data: x_data,
        };

        let mut complex_data: Vec<Complex<f32>> = data.into_iter().map(|v| {
            Complex::new(*v, 0.0f32)
        }).collect();
        let mut planner = FFTplanner::new(false);
        let fft = planner.plan_fft(complex_data.len());
        fft.process(&mut complex_data[..], &mut fft_output.fft_data);

        return fft_output;
    }

    pub fn fft_shift(&mut self) {

        let x_data_len = f32::ceil(self.x_data.len() as f32 / 2.0);

        self.x_data = self.x_data[0..x_data_len as usize].to_vec();
        self.fft_data = self.fft_data[0..x_data_len as usize].to_vec();
    }
}

#[cfg(test)]
mod tests {
    use crate::{FftData};
    use rustfft::num_complex::Complex;

    #[test]
    fn test_fft() {
        let data: Vec<f32> = vec![10.0, 5.0, 100.0, 0.0, 1.0];
        let fft_data = FftData::from_slice(&data[..], 0.1f32);
        assert_eq!(fft_data.x_data, vec![0.0, 2.0, 4.0, 6.0, 8.0]);
        assert_eq!(fft_data.fft_data, vec![Complex { re: 116.0, im: 0.0 },
                                           Complex { re: -69.0476, im: -62.582752 },
                                           Complex { re: 36.0476, im: 92.75451 },
                                           Complex { re: 36.0476, im: -92.75451 },
                                           Complex { re: -69.0476, im: 62.582752 }]);
    }

    #[test]
    fn test_fft_shift_even() {
        let data: Vec<f32> = vec![10.0, 5.0, 100.0, 0.0];
        let mut fft_data = FftData::from_slice(&data[..], 0.1f32);

        fft_data.fft_shift();

        assert_eq!(fft_data.fft_data.len(), fft_data.x_data.len());
        assert_eq!(fft_data.x_data, vec![0.0, 2.5]);
        assert_eq!(fft_data.fft_data, vec![
            Complex { re: 115.0, im: 0.0 }, Complex { re: -90.0, im: -5.0 }
        ]);
    }

    #[test]
    fn test_fft_shift_uneven() {
        let data: Vec<f32> = vec![10.0, 5.0, 100.0, 0.0, 1.0];
        let mut fft_data = FftData::from_slice(&data[..], 0.1f32);

        fft_data.fft_shift();

        assert_eq!(fft_data.fft_data.len(), fft_data.x_data.len());
        assert_eq!(fft_data.x_data, vec![0.0, 2.0, 4.0]);
        assert_eq!(fft_data.fft_data, vec![
            Complex { re: 116.0, im: 0.0 },
            Complex { re: -69.0476, im: -62.582752 },
            Complex { re: 36.0476, im: 92.75451 },
        ]);
    }
}
